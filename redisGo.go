package main

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
)

func main() {

	pool := newPool()
	conn := pool.Get()
	defer conn.Close()
	err := ping(conn)
	if err != nil {
		fmt.Println(err)
	}
	err = set(conn)
	if err != nil {
		fmt.Println(err)
	}
	err = get(conn)
	if err != nil {
		fmt.Println(err)
	}
}

func newPool() *redis.Pool {
	return &redis.Pool{
		MaxIdle: 80,
		MaxActive: 12000,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", ":3306")
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}
}

func ping(c redis.Conn) error {
	s, err := redis.String(c.Do("PING"))
	if err != nil {
		return err
	}

	fmt.Printf("PING Response = %s\n", s)
	return nil
}

func set(c redis.Conn) error {
	_, err := c.Do("SET", "Sonik", "40")
	if err != nil {
		return err
	}
	_, err = c.Do("SET", "Hamster", 300)
	if err != nil {
		return err
	}
	return nil
}

func get(c redis.Conn) error {
	key := "Sonik"
	s, err := redis.String(c.Do("GET", key))
	if err != nil {
		return (err)
	}
	fmt.Printf("%s = %s\n", key, s)

	key = "Hamster"
	i, err := redis.Int(c.Do("GET", key))
	if err != nil {
		return (err)
	}
	fmt.Printf("%s = %d\n", key, i)
	return nil
}

