package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

type product struct {
	id    int
	type_ string
	price int
}

type customer struct {
	id    int
	name_ string
	age   int
}

func insert(db *sql.DB) {
	result, err := db.Exec("insert into zoo.products (id, type_, price) values ('1', 'Parrot', 100)")
	result, err = db.Exec("insert into zoo.products (id, type_, price) values ('2', 'Dog', 400)")
	result, err = db.Exec("insert into zoo.products (id, type_, price) values ('3', 'Cat', 300)")
	result, err = db.Exec("insert into zoo.products (id, type_, price) values ('4', 'Hamster', 200)")
	result, err = db.Exec("insert into zoo.customers (id, name_, age) values ('1', 'Gordon', 25)")
	result, err = db.Exec("insert into zoo.customers (id, name_, age) values ('2', 'Sonik', 76)")
	if err != nil {
		panic(err)
	}
	fmt.Println(result.LastInsertId())
	fmt.Println(result.RowsAffected())
}
func update(db *sql.DB) {
		result, err := db.Exec("update zoo.products set price = 400 where type_ = 'Parrot'")
		if err != nil{
			panic(err)
		}
		result, err = db.Exec("update zoo.customers set age = 40 where name_ = 'Sonik'")
		if err != nil{
			panic(err)
		}
		fmt.Println(result.LastInsertId())
		fmt.Println(result.RowsAffected())
}
func delete(db *sql.DB) {
	result, err := db.Exec("delete from zoo.customers where name_ = 'Sonik'")
	if err != nil{
		panic(err)
	}
	result, err = db.Exec("delete from zoo.products where type_ = 'Dog'")
	if err != nil{
		panic(err)
	}
	fmt.Println(result.LastInsertId())
	fmt.Println(result.RowsAffected())
}
func getAllProducts(db *sql.DB) {
	rows, err := db.Query("select * from zoo.products")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	products := []product{}

	for rows.Next() {
		p := product{}
		err := rows.Scan(&p.id, &p.type_, &p.price)
		if err != nil {
			fmt.Println(err)
			continue
		}
		products = append(products, p)
	}
	for _, p := range products {
		fmt.Println(p.id, p.type_, p.price)
	}
}
func getAllUsers(db *sql.DB){
	rowsCust, err := db.Query("select * from zoo.customers")
	if err != nil {
		panic(err)
	}
	defer rowsCust.Close()
	customers := []customer{}

	for rowsCust.Next() {
		c := customer{}
		err := rowsCust.Scan(&c.id, &c.name_, &c.age)
		if err != nil {
			fmt.Println(err)
			continue
		}
		customers = append(customers, c)
	}
	for _, c := range customers {
		fmt.Println(c.id, c.name_, c.age)
	}
}

func main() {
	db, err := sql.Open("mysql", "root:11111@tcp(127.0.0.1:3306)/zoo")
	if err != nil {
		panic(err)
	}
	defer db.Close()
	fmt.Println("After insert:")
	insert(db)
	getAllProducts(db)
	getAllUsers(db)
	fmt.Println("After update")
	update(db)
	getAllProducts(db)
	getAllUsers(db)
	fmt.Println("After delete")
	delete(db)
	getAllProducts(db)
	getAllUsers(db)
}
